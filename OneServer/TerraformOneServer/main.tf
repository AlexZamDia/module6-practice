terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone      = "ru-central1-a"
}

# Получаем идентификатор последнего образа Ubuntu
data "yandex_compute_image" "last_ubuntu" {
  family = "ubuntu-2204-lts" # Ubuntu 22.04 LTS
}

# Найдем сеть по-умолчанию вместо создания новой
data "yandex_vpc_subnet" "default_a" {
  name = "default-ru-central1-a" # одна из дефолтных подсетей
}

resource "yandex_compute_instance" "my-webserver" {
  name = "reactjs-server"
  #platform_id = "standard-v1" # тип процессора (Intel Broadwell)

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      #image_id = "fd8emvfmfoaordspe1jr" # Ubuntu 22.04 LTS
      image_id = data.yandex_compute_image.last_ubuntu.id
      size     = "8"
    }
  }

  network_interface {
    #subnet_id = yandex_vpc_subnet.subnet-1.id
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}

#resource "yandex_vpc_network" "network-1" {
#  name = "network1"
#}

#resource "yandex_vpc_subnet" "subnet-1" {
#  name           = "subnet1"
#  zone           = "ru-central1-a"
#  network_id     = yandex_vpc_network.network-1.id
#  v4_cidr_blocks = ["192.168.10.0/24"]
#}

output "internal_ip_address_my_webserver" {
  value = yandex_compute_instance.my-webserver.network_interface.0.ip_address
}


output "external_ip_address_my_webserver" {
  value = yandex_compute_instance.my-webserver.network_interface.0.nat_ip_address
}
