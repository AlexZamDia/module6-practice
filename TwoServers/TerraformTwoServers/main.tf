terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-a"
}

# Получаем идентификатор последнего образа Ubuntu
data "yandex_compute_image" "last_ubuntu" {
  family = "ubuntu-2204-lts" # Ubuntu 22.04 LTS
}

# Найдем сеть по-умолчанию вместо создания новой
data "yandex_vpc_subnet" "default_a" {
  name = "default-ru-central1-a" # одна из дефолтных подсетей
}

resource "yandex_iam_service_account" "ig_sa" {
  name        = "ig-sa"
  description = "service account to manage ig"
}

resource "yandex_resourcemanager_folder_iam_binding" "admin" {
  #folder_id = "${var.folder_id}"
  folder_id = "b1gu3sd70a2r2d7n01vc"
  role      = "admin"
  members = [
    "serviceAccount:${yandex_iam_service_account.ig_sa.id}",
  ]
  depends_on = [
    yandex_iam_service_account.ig_sa,
  ]
}

resource "yandex_iam_service_account_static_access_key" "static-access-key" {
  service_account_id = yandex_iam_service_account.ig_sa.id
  depends_on = [
    yandex_iam_service_account.ig_sa,
  ]
}


/*
resource "yandex_compute_instance" "web1" {
  name = "web1"
  #platform_id = "standard-v1" # тип процессора (Intel Broadwell)

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      #image_id = "fd8emvfmfoaordspe1jr" # Ubuntu 22.04 LTS
      image_id = data.yandex_compute_image.last_ubuntu.id
      size     = "8"
    }
  }

  network_interface {
    #subnet_id = yandex_vpc_subnet.subnet-1.id
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}

resource "yandex_compute_instance" "web2" {
  name = "web2"
  #platform_id = "standard-v1" # тип процессора (Intel Broadwell)

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      #image_id = "fd8emvfmfoaordspe1jr" # Ubuntu 22.04 LTS
      image_id = data.yandex_compute_image.last_ubuntu.id
      size     = "8"
    }
  }

  network_interface {
    #subnet_id = yandex_vpc_subnet.subnet-1.id
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}
*/

# Группа ВМ
resource "yandex_compute_instance_group" "webgroup" {
  name = "webgroup"
  #service_account_id = yandex_iam_service_account.admin.id
  service_account_id = yandex_iam_service_account.ig_sa.id
  depends_on = [
    yandex_iam_service_account.ig_sa
  ]

  load_balancer {
    target_group_name = "webgroup"
  }

  instance_template {
    name = "web-{instance.index}"

    resources {
      cores  = 2
      memory = 2
      #core_fraction = 20
    }

    boot_disk {
      initialize_params {
        image_id = data.yandex_compute_image.last_ubuntu.id
        size     = 8
        #type     = "network-ssd"
      }
    }

    network_interface {
      subnet_ids = [data.yandex_vpc_subnet.default_a.subnet_id]
      nat        = true
    }

    metadata = {
      ssh-keys  = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
      user-data = "${file("user_data.sh")}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 3
    }
  }

  allocation_policy {
    /*
    zones = [
      "ru-central1-a",
      "ru-central1-b",
      "ru-central1-c",
    ]
    */
    zones = [
      "ru-central1-a"
    ]
  }

  deploy_policy {
    max_unavailable = 3
    max_creating    = 3
    max_expansion   = 3
    max_deleting    = 3
  }
}

/*
data "yandex_lb_target_group" "webserverslb" {
  name = yandex_compute_instance_group.webgroup.load_balancer[0].target_group_name
} 
*/

resource "yandex_lb_network_load_balancer" "lbweb" {
  name = "lbweb"
  depends_on = [
    yandex_compute_instance_group.webgroup
  ]

  listener {
    name = "listener-web-servers"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    #target_group_id = yandex_lb_target_group.web-servers.id
    #target_group_id = data.yandex_lb_target_group.webserverslb.id
    target_group_id = yandex_compute_instance_group.webgroup.load_balancer.0.target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

/*
resource "yandex_lb_target_group" "web-servers" {
  name = "web-servers-target-group"
 
  target {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    address   = yandex_compute_instance.web1.network_interface.0.ip_address
  }
 
  target {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    address   = yandex_compute_instance.web2.network_interface.0.ip_address
  }
}
*/

/*
output "internal_ip_address_web1" {
  value = yandex_compute_instance.web1.network_interface.0.ip_address
}

output "external_ip_address_web1" {
  value = yandex_compute_instance.web1.network_interface.0.nat_ip_address
}

output "internal_ip_address_web2" {
  value = yandex_compute_instance.web2.network_interface.0.ip_address
}

output "external_ip_address_web2" {
  value = yandex_compute_instance.web2.network_interface.0.nat_ip_address
}
*/

output "instance_group_webgroup_public_ips" {
  description = "Public IP addresses:"
  value       = yandex_compute_instance_group.webgroup.instances.*.network_interface.0.nat_ip_address
}

output "instance_group_webgroup_private_ip" {
  description = "Private IP addresses:"
  value       = yandex_compute_instance_group.webgroup.instances.*.network_interface.0.ip_address
}

output "lb_ip_address" {
  value = yandex_lb_network_load_balancer.lbweb.*
}